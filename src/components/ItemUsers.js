import React from 'react';

const ItemUser = ({user}) =>{
  console.log(user);
    return (
      <div style={divStyle} >
        <footer style={footertyle} >
            <ul style={ulStyle} >
              <li>
                <p style={titleStyle} >{user.name}</p>
                <p style={pStyle} >{user.description}</p>
                <p style={pStyle} >{user.language}</p> 
              </li>
            </ul>
          </footer>
      </div>
    );
}

const divStyle = {
  float: 'left',
  width: '48%',
  height: '60px',
  margin: '10px',
  padding: '50px 0',
  textAlign: 'center'

};

const footertyle = {
  background: '#fff',
  border: '1px solid #d1d5da',
  borderRadius: '3px',
  textAlign: 'left',
  height: '150px'
};

const ulStyle = {
  listStyleType: 'none',
  marginTop: '10px',

};

const titleStyle = {
  fontSize: '18px',
  color: '#0366d6'
};

const pStyle = {
  fontSize: '16px',
};

export default ItemUser;
